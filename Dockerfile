# Build remco from specific commit
######################################################
FROM golang AS remco

# remco (lightweight configuration management tool) https://github.com/HeavyHorst/remco
RUN go install github.com/HeavyHorst/remco/cmd/remco@latest


# Build enshrouded base
######################################################
FROM ubuntu:oracular AS enshrouded-base
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV ENSHROUDED_HOME=/home/enshrouded
ENV ENSHROUDED_UID=10000
ENV ENSHROUDED_GID=10000

USER root

## Install system packages
RUN dpkg --add-architecture amd64
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install \
    curl \
    vim \
    ca-certificates \
    file \
    lib32gcc-s1 \
    wget \
    libvulkan1 && \
    apt-get clean all

RUN mkdir -pm755 /etc/apt/keyrings && \
    wget --progress=dot:giga -O /etc/apt/keyrings/winehq-archive.key https://dl.winehq.org/wine-builds/winehq.key && \
    wget --progress=dot:giga -NP /etc/apt/sources.list.d/ https://dl.winehq.org/wine-builds/ubuntu/dists/noble/winehq-noble.sources && \
    dpkg --add-architecture i386 && \
    apt-get -y update && apt-get -y install --install-recommends winehq-staging

## Create enshrouded user and group
RUN groupadd -g $ENSHROUDED_GID enshrouded && \
    useradd -l -s /bin/bash -d $ENSHROUDED_HOME -m -u $ENSHROUDED_UID -g enshrouded enshrouded && \
    passwd -d enshrouded

## Install remco
COPY --from=remco /go/bin/remco /usr/local/bin/remco
COPY --chown=enshrouded:root remco /etc/remco
RUN chmod -R 0775 /etc/remco

## Install entrypoint
COPY --chown=enshrouded:enshrouded files/entrypoint.sh ${ENSHROUDED_HOME}/

## Update permissions
RUN chmod ugo+x ${ENSHROUDED_HOME}/entrypoint.sh && \
    chown -R enshrouded:enshrouded ${ENSHROUDED_HOME}


# Build enshrouded image
######################################################
FROM enshrouded-base as enshrouded

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV ENSHROUDED_HOME=/home/enshrouded
ENV ENSHROUDED_STEAM_VALIDATE="false"
ENV REMCO_HOME=/etc/remco
ENV STEAMAPPID=2278520

ARG CI_COMMIT_AUTHOR
ARG CI_COMMIT_TIMESTAMP
ARG CI_COMMIT_SHA
ARG CI_COMMIT_TAG
ARG CI_PROJECT_URL

LABEL maintainer=$CI_COMMIT_AUTHOR
LABEL author=nathan.snow@mimir-tech.org
LABEL description="Enshrouded dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="registry.gitlab.com/japtain_cack/enshrouded-server"
LABEL org.label-schema.description="Enshrouded dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.url=$CI_PROJECT_URL
LABEL org.label-schema.vcs-url=$CI_PROJECT_URL
LABEL org.label-schema.docker.cmd="docker run -it -d -v /mnt/enshrouded/world1/:/home/enshrouded/server/ -p 7777/udp -p 27015/udp -p 18888/tcp registry.gitlab.com/japtain_cack/enshrouded-server"
LABEL org.label-schema.vcs-ref=$CI_COMMIT_SHA
LABEL org.label-schema.version=$CI_COMMIT_TAG
LABEL org.label-schema.build-date=$CI_COMMIT_TIMESTAMP

WORKDIR ${ENSHROUDED_HOME}
VOLUME "${ENSHROUDED_HOME}/server"
# server ports
EXPOSE 15636-15637/tcp
EXPOSE 15636-15637/udp

USER enshrouded
ENTRYPOINT ["./entrypoint.sh"]
