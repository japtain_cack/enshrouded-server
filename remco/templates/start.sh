./enshrouded_server.exe \
  -SteamServerName="{{ getv("/enshrouded/servername", "Enshrouded Dedicated Server") }}" \
  -Port="{{ getv("/enshrouded/port", "8777") }}" \
  -QueryPort="{{ getv("/enshrouded/queryport", "27015") }}" \
  -EchoPort="{{ getv("/enshrouded/echoyport", "18888") }}" \
{% if getv("/enshrouded/pve", "false")|lower == "true" %}
  -pve \
{% endif %}
  -MaxPlayers="{{ getv("/enshrouded/maxplayers", "50") }}" \
  -PSW="{{ getv("/enshrouded/password", "") }}"
  -adminpsw="{{ getv("/enshrouded/admin/password", "Secret123") }}"
