#!/usr/bin/env bash

export REMCO_RESOURCE_DIR=${REMCO_HOME}/resources.d
export REMCO_TEMPLATE_DIR=${REMCO_HOME}/templates
export LD_LIBRARY_PATH=/home/enshrouded/server/linux64:/home/enshrouded/server/linux32:/home/enshrouded/server/steamcmd/linux64:/home/enshrouded/server/steamcmd/linux32:/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH
export WINEARCH=win64
export WINEPREFIX=${ENSHROUDED_HOME}/server/.wine
export HOME=${ENSHROUDED_HOME}/server

echo "#####################################"
date
echo

chown -Rv enshrouded:enshrouded /home/enshrouded

if [[ ! -z $ENSHROUDED_STEAM_VALIDATE ]]; then
  if [[ ! "$ENSHROUDED_STEAM_VALIDATE" =~ true|false ]]; then
    echo '[ERROR] ENSHROUDED_STEAM_VALIDATE must be true or false'
    exit 1
  elif [[ "$ENSHROUDED_STEAM_VALIDATE" == true ]]; then
    ENSHROUDED_STEAM_VALIDATE_VALUE="validate"
  else
    ENSHROUDED_STEAM_VALIDATE_VALUE=""
  fi
fi

mkdir -p server/steamcmd/
curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - -C ${ENSHROUDED_HOME}/server/steamcmd/
chmod ugo+x ${ENSHROUDED_HOME}/server/steamcmd/steamcmd.sh

wget -O ${ENSHROUDED_HOME}/server/winetricks https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks

#Make .sh executable
chmod ugo+x ${ENSHROUDED_HOME}/server/winetricks

cat <<EOF> ${ENSHROUDED_HOME}/server/enshrouded.conf
@ShutdownOnFailedCommand 1
@NoPromptForPassword 1
@sSteamCmdForcePlatformType windows
force_install_dir ${ENSHROUDED_HOME}/server
login anonymous
app_update ${STEAMAPPID} ${ENSHROUDED_STEAM_VALIDATE_VALUE}
quit
EOF

cat << EOF >> ${ENSHROUDED_HOME}/server/winetricks.sh
#!/bin/bash
export DISPLAY=:1.0
Xvfb :1 -screen 0 1024x768x16 &
env WINEDLLOVERRIDES="mscoree=d" wineboot --init /nogui
winetricks corefonts
winetricks sound=disabled
winetricks -q --force vcrun2022
wine winecfg -v win10
rm -rf ${ENSHROUDED_HOME}/server/.cache
EOF
chmod ugo+x ${ENSHROUDED_HOME}/server/winetricks.sh

cd ${ENSHROUDED_HOME}/server && \
steamcmd/steamcmd.sh +runscript ${ENSHROUDED_HOME}/server/enshrouded.conf

remco

echo
echo "#####################################"
echo starting server...
echo

# Use Wine to run the server
wine64 ./start.sh
